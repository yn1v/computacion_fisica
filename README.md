Este es un conjunto de materiales para enseñar los conceptos básicos de computación física.

Parte de los diseños usados fueron tomados de la librería de Fritzing de Adafruit
https://github.com/adafruit/Fritzing-Library

Los ejemplos se desarrollan principalmente usando la librería gpiozero
https://gpiozero.readthedocs.io

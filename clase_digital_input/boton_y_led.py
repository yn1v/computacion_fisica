from gpiozero import LED, Button
from signal import pause

verde = LED(16)
boton = Button(26)

boton.when_pressed = verde.on
boton.when_released = verde.off

pause()

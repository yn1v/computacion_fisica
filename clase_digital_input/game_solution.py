from gpiozero import Button, LED
from time import time, sleep
from random import randint

led = LED(16)
btn = Button(26)

sleep(randint(1,5))
led.on()
start = time()
btn.wait_for_press()
led.off()
end = time()
print(end-start)


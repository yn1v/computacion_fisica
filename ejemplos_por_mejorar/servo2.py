from gpiozero import AngularServo
from time import sleep

servo = AngularServo(17, min_angle=0, max_angle=90)
pausa = 4
while True:
    servo.angle = 0
    sleep(pausa)
    servo.angle = 30
    sleep(pausa)
    servo.angle = 45
    sleep(pausa)
    servo.angle = 60
    sleep(pausa)
    servo.angle = 90
    sleep(pausa)

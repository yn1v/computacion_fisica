Raspberry Pi es una computadora que apunta a brindar las oportunidades de aprendizaje de finales de los 70's e inicios de los 80's

Los puertos seriales universales (USB) tienen ventajas y desventajas.

Señales digitales son ambiguas. Encendido/Apagado vs. Codificación digital.

Ls puertos USB solo transmiten codificación digital. Al no tener puertos seriales o paralelos no podemos enviar o recibir señales simples.

Un microcontrolador puede ser un puente para el mundo físico. Se requiere un firmware en el microcontrolador que mantenga una comunicación abierta y programar hacia esa comunicación serial encapsulada dentro del puerto USB. No es el punto fuerte de un microcontrolador.

Raspberry Pi con la propuesta de GPIO permite una gran flexibilidad al tener acceso a señales digitales simples y codificadas, directamente desde el microprocesador.

Limitaciones: Del Raspberry pi. Conversión Analógico Digital, Hardware de Reloj y procesamiento en tiempo real.

El bread board. Inicios de las tablas de cortar de madera, con clavos y alambres de cobre.

Logica de columnas. Rieles de poder.

Ejemplo: Un boton.

El LED es un componente polarizado. La resistencia regula la cantidad de corriente disponible. El pin más corto es negativo o tierra, que es el lado plano.

La resistencia puede estar en cualquier polo, no son polarizadas.

Para que un circuito funcione se requiere cerrar el circuito. Un corto, es un circuito que funciona de una manera inesperada. 

Ejemplo: Dos botones.

Que ventajas tenemos de usar un elemento programable para encender un LED?

Ejemplo de código: un_led.py

Cumplir secuencia de programa. El hola mundo de la cibernetica es encender y apagar un LED.

Que es un relé o relvador? (Relay). 

Si tenemos una señal que enciende un LED, podemos controlar un relé.

Control por modulación de amplitud de pulso. Pulso Wide Modulation.
Frecuencia y ciclo de trabajo (Duty Cycle). También se usa régimen de trabajo

Ejercicio: Con un ciclo de 50% a que frecuencia dejamos de percibir el cambio entre apagado y encendido?

Intensidad de brillo de un LED se puede controlar por PWM. Aclaración de PWM por hardware y por software. Velocidad de un motor. Transistores y reles de estado sólido.

Ejemplo de código: intensidad.py

Mutiples LEDS. Es más de lo mismo.

Ejercicio: controlesmos los 3 leds.

Ejemplo: semaforo.py

Que pasa si usamos leds rojo, verde y azul y los cubrimos con un material difuso?

Cambiemos a un LED RGB. Posibilidad de cátodo(-)/ánodo(+) común. Es decir si el LED se controla por la señales positivas o negativas. Lo más comun es control por señales positivas, es decir cátodo común. El negativo o tierra es el pin más largo.

Ejemplo: rgb.py


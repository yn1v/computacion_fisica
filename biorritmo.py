from math import sin, pi
from time import sleep

# Formulas de Biorritmo
fisico = 2 * pi / 23
emocional = 2 * pi / 28
intelectual = 2 * pi / 33

# Las ondas sinusoidales van de 1 a -1
# Se requiere una amplitud de 255, que equivale a 255/2 a -255/2
limite = int(255/2)

# El ciclo se va a completar con la coincidencia de los valores
# No hay factores comunes
# 23  primo
# 28 = 2 * 2 * 7 = 4 * 7
# 33 = 3 * 11
ciclo = 23 * 28 * 33

for i in range(ciclo):
    rojo = int(sin(emocional*i) * limite)
    verde = int(sin(fisico*i) * limite)
    azul = int(sin(intelectual*i) * limite)

# El rango es de 255, pero va de 127 a -127 y necesitamos numeros positivos
    print((rojo+limite), "\t", (verde+limite), "\t", (azul+limite))
    sleep(0.5)


